const electron = require('electron');
const app = electron.app;
const {BrowserWindow} = electron;

app.on('ready', function(){
    let mainWindow = new BrowserWindow({
        name: 'seriously',
        width: 1280,
        height: 960,
        toolbar: false,
        resizable: false
    })

    mainWindow.setMenu(null);

    mainWindow.loadURL('file://' + __dirname + '/index.html');
    
    mainWindow.webContents.openDevTools({
        detach: false
    })

    mainWindow.on('closed', function () {
        mainWindow = null;
    });
});