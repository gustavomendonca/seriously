const utils = require('./utils.js');
const seriously = new Seriously();
var src;
var target;

var video;
var slider;

function setup() {
    var effects = utils.getEffects();
    canvas = createCanvas(windowWidth / 2, windowHeight / 2, WEBGL);
    canvas.id('p5canvas');
    background(51);
    video = createCapture(VIDEO);
    video.id('p5video');
    video.size(windowWidth / 2, windowHeight / 2);
    video.hide();
    slider = createSlider(0, 1, 0.5, 0.01);
    slider.id('effect-slider');

    src = seriously.source('#p5video');
    target = seriously.target('#p5canvas');

    listbox = createSelect();
    listbox.position(10, 10);

    effects.forEach(eff => {
        listbox.option(eff.substring(10, eff.length - 3));
    });

    listbox.changed(function() {
        applyEffect(listbox.value());
    });

    applyEffect(listbox.value());
    seriously.go();

}

function applyEffect(effectName) {
    var effect = seriously.effect(effectName);
    effect.amount = '#effect-slider';
    effect.source = src;
    target.source = effect;
}

function draw() {

}